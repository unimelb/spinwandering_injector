# spinwandering_injector
This repository includes a few wrapper Python scripts that can perform spin-wandering injections, as described in Carlin & Melatos 2024 ([DCC link](https://dcc.ligo.org/P2400149)). The scripts are designed to be run from the command line, with a variety of arguments available.

# Running/environment
A minimal scientific python environment is required, as specified in requirements.txt.

The script `injector.py` will produce calls to `lalpulsar_MakeFakeData_v5` if asked, as such, the user's environment should include this routine.

# Minimal instructions for use
The code has in-line comments, and has arguments that should be mostly self-explanatory if you run them with `--help`. Many injection parameters are kept in an ini file, an example of which is in the `example_files` folder.

A simple example of using the code is the two invocations:

```
python freq_track_maker.py --which=uniformfddot --save_dir=. --ws=1 --inj_ini=example_files/inj_ini.ini

python injector.py --path_file=ftrack_uniformfddot.txt --out_dir=sft_dir --h0=1e-25 --inj_ini=example_files/inj_ini.ini
```

The first call would create a file `ftrack_uniformfddot.txt`, based on the injection parameters defined in `example_files/inj_ini.ini`, which has a header that includes injection parameters, and then four columns that hold the phase, freq, fdot, and fddot at each increment of tsft. The second call would inject that sequence of phase, freq, fdot, and fddot, into Gaussian noise, using `MakeFakeData_v5`.

